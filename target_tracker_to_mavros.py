import math

import rospy
import mavros_msgs
from std_msgs.msg import Float64MultiArray
from mavros_msgs.msg import PositionTarget


publish_cmd = PositionTarget()


def target_tracker_callback(data):
    publish_cmd.header.stamp = rospy.Time.now();
    publish_cmd.header.frame_id = "world";
    publish_cmd.header.seq += 1;
    publish_cmd.coordinate_frame = mavros_msgs.msg.PositionTarget.FRAME_BODY_NED;

    publish_cmd.type_mask = 1991;

    publish_cmd.velocity.x = data.data[0] / 100;
    publish_cmd.velocity.y = data.data[1] / 100;
    publish_cmd.velocity.z = data.data[2] / 100;

    publish_cmd.yaw_rate = data.data[3] * (math.pi/180.0) ;


if __name__ == '__main__':
    try:
        rospy.init_node('traget_tracker_to_mavros', anonymous=True)
        pub = rospy.Publisher('mavros/setpoint_raw/local', PositionTarget, queue_size=100)
        rospy.Subscriber("traget_tracker_to_mavros", Float64MultiArray, target_tracker_callback)

        rate = rospy.Rate(20)  # 10hz

        while not rospy.is_shutdown():
            rospy.loginfo(publish_cmd)
            pub.publish(publish_cmd)
            rate.sleep()
        rospy.signal_shutdown("Shutdown")
    except rospy.ROSInterruptException:
        pass