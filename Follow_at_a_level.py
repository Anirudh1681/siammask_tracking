from __future__ import division

import time
import numpy as np
import cv2
import rospy
from std_msgs.msg import Float64MultiArray

from PIL import Image
from io import BytesIO
import requests

from os.path import join
import json
import glob
import os

from core.siammask.utils.load_helper import load_pretrain
from core.siammask.utils.bbox_helper import get_axis_aligned_bbox, cxy_wh_2_rect

import torch
from torch.autograd import Variable
import torch.nn.functional as F

from core.siammask.utils.anchors import Anchors
from core.siammask.utils.tracker_config import TrackerConfig


tracker_output_cmd_vel = Float64MultiArray()

def to_torch(ndarray):
    if type(ndarray).__module__ == 'numpy':
        return torch.from_numpy(ndarray)
    elif not torch.is_tensor(ndarray):
        raise ValueError("Cannot convert {} to torch tensor"
                         .format(type(ndarray)))
    return ndarray


def im_to_torch(img):
    img = np.transpose(img, (2, 0, 1))  # C*H*W
    img = to_torch(img).float()
    return img


def get_subwindow_tracking(im, pos, model_sz, original_sz, avg_chans, out_mode='torch'):
    '''
    Crops the image around a point and if the crop is out of edge of the image, it
    fills it with a fill color.
    params:
        im: np.array, image to be cropped
        pos: np.array, center of the crop
        model_sz: int, output size of the crop after resizing
        original_sz: int, size of the crop in the image
        avg_channels: np.array, fill color used to fill crop when it goes out of bounds
        out_mode: string, return image as numpy or tensor torch
    example: z = get_subwindow_tracking(im, np.array([375. , 138.5]), 127, 100,
            avg_chans = np.array([ 96, 114, 141]), out_mode='torch')
    '''

    if isinstance(pos, float):
        pos = [pos, pos]
    sz = original_sz
    im_sz = im.shape
    c = (original_sz + 1) / 2
    context_xmin = round(pos[0] - c)
    context_xmax = context_xmin + sz - 1
    context_ymin = round(pos[1] - c)
    context_ymax = context_ymin + sz - 1
    left_pad = int(max(0., -context_xmin))
    top_pad = int(max(0., -context_ymin))
    right_pad = int(max(0., context_xmax - im_sz[1] + 1))
    bottom_pad = int(max(0., context_ymax - im_sz[0] + 1))

    context_xmin = context_xmin + left_pad
    context_xmax = context_xmax + left_pad
    context_ymin = context_ymin + top_pad
    context_ymax = context_ymax + top_pad

    # zzp: a more easy speed version
    r, c, k = im.shape
    if any([top_pad, bottom_pad, left_pad, right_pad]):
        te_im = np.zeros((r + top_pad + bottom_pad, c + left_pad + right_pad, k), np.uint8)
        te_im[top_pad:top_pad + r, left_pad:left_pad + c, :] = im
        if top_pad:
            te_im[0:top_pad, left_pad:left_pad + c, :] = avg_chans
        if bottom_pad:
            te_im[r + top_pad:, left_pad:left_pad + c, :] = avg_chans
        if left_pad:
            te_im[:, 0:left_pad, :] = avg_chans
        if right_pad:
            te_im[:, c + left_pad:, :] = avg_chans
        im_patch_original = te_im[int(context_ymin):int(context_ymax + 1), int(context_xmin):int(context_xmax + 1), :]
    else:
        im_patch_original = im[int(context_ymin):int(context_ymax + 1), int(context_xmin):int(context_xmax + 1), :]

    if not np.array_equal(model_sz, original_sz):
        im_patch = cv2.resize(im_patch_original, (model_sz, model_sz))
    else:
        im_patch = im_patch_original
    # cv2.imshow('crop', im_patch)
    # cv2.waitKey(0)
    return im_to_torch(im_patch) if out_mode in 'torch' else im_patch


def generate_anchor(cfg, score_size):
    anchors = Anchors(cfg)
    anchor = anchors.anchors
    x1, y1, x2, y2 = anchor[:, 0], anchor[:, 1], anchor[:, 2], anchor[:, 3]
    anchor = np.stack([(x1 + x2) * 0.5, (y1 + y2) * 0.5, x2 - x1, y2 - y1], 1)

    total_stride = anchors.stride
    anchor_num = anchor.shape[0]

    anchor = np.tile(anchor, score_size * score_size).reshape((-1, 4))
    ori = - (score_size // 2) * total_stride
    xx, yy = np.meshgrid([ori + total_stride * dx for dx in range(score_size)],
                         [ori + total_stride * dy for dy in range(score_size)])
    xx, yy = np.tile(xx.flatten(), (anchor_num, 1)).flatten(), \
             np.tile(yy.flatten(), (anchor_num, 1)).flatten()
    anchor[:, 0], anchor[:, 1] = xx.astype(np.float32), yy.astype(np.float32)
    return anchor


def siamese_init(im, target_pos, target_sz, model, hp=None, device='cpu'):
    state = dict()
    state['im_h'] = im.shape[0]
    state['im_w'] = im.shape[1]
    p = TrackerConfig()
    p.update(hp, model.anchors)

    p.renew()

    net = model
    p.scales = model.anchors['scales']
    p.ratios = model.anchors['ratios']
    p.anchor_num = model.anchor_num
    p.anchor = generate_anchor(model.anchors, p.score_size)
    avg_chans = np.mean(im, axis=(0, 1))

    wc_z = target_sz[0] + p.context_amount * sum(target_sz)
    hc_z = target_sz[1] + p.context_amount * sum(target_sz)
    s_z = round(np.sqrt(wc_z * hc_z))
    # initialize the exemplar
    z_crop = get_subwindow_tracking(im, target_pos, p.exemplar_size, s_z, avg_chans)

    z = Variable(z_crop.unsqueeze(0))
    net.template(z.to(device))

    if p.windowing == 'cosine':
        window = np.outer(np.hanning(p.score_size), np.hanning(p.score_size))
    elif p.windowing == 'uniform':
        window = np.ones((p.score_size, p.score_size))
    window = np.tile(window.flatten(), p.anchor_num)

    state['p'] = p
    state['net'] = net
    state['avg_chans'] = avg_chans
    state['window'] = window
    state['target_pos'] = target_pos
    state['target_sz'] = target_sz
    return state


def siamese_track(state, im, mask_enable=False, refine_enable=False, device='cpu', debug=False):
    p = state['p']
    net = state['net']
    avg_chans = state['avg_chans']
    window = state['window']
    target_pos = state['target_pos']
    target_sz = state['target_sz']

    wc_x = target_sz[1] + p.context_amount * sum(target_sz)
    hc_x = target_sz[0] + p.context_amount * sum(target_sz)
    s_x = np.sqrt(wc_x * hc_x)
    scale_x = p.exemplar_size / s_x
    d_search = (p.instance_size - p.exemplar_size) / 2
    pad = d_search / scale_x
    s_x = s_x + 2 * pad
    crop_box = [target_pos[0] - round(s_x) / 2, target_pos[1] - round(s_x) / 2, round(s_x), round(s_x)]

    if debug:
        im_debug = im.copy()
        crop_box_int = np.int0(crop_box)
        cv2.rectangle(im_debug, (crop_box_int[0], crop_box_int[1]),
                      (crop_box_int[0] + crop_box_int[2], crop_box_int[1] + crop_box_int[3]), (255, 0, 0), 2)
        cv2.imshow('search area', im_debug)
        cv2.waitKey(0)

    # extract scaled crops for search region x at previous target position
    x_crop = Variable(get_subwindow_tracking(im, target_pos, p.instance_size, round(s_x), avg_chans).unsqueeze(0))
    # x_crop = Variable(get_subwindow_tracking(im, np.array(im.shape[:2])//2, p.instance_size, 300, avg_chans).unsqueeze(0))

    if mask_enable:
        score, delta, mask = net.track_mask(x_crop.to(device))
    else:
        score, delta = net.track(x_crop.to(device))

    delta = delta.permute(1, 2, 3, 0).contiguous().view(4, -1).data.cpu().numpy()
    score = F.softmax(score.permute(1, 2, 3, 0).contiguous().view(2, -1).permute(1, 0), dim=1).data[:,
            1].cpu().numpy()

    delta[0, :] = delta[0, :] * p.anchor[:, 2] + p.anchor[:, 0]
    delta[1, :] = delta[1, :] * p.anchor[:, 3] + p.anchor[:, 1]

    delta[2, :] = np.exp(delta[2, :]) * p.anchor[:, 2]
    delta[3, :] = np.exp(delta[3, :]) * p.anchor[:, 3]

    def change(r):
        return np.maximum(r, 1. / r)

    def sz(w, h):
        pad = (w + h) * 0.5
        sz2 = (w + pad) * (h + pad)
        return np.sqrt(sz2)

    def sz_wh(wh):
        pad = (wh[0] + wh[1]) * 0.5
        sz2 = (wh[0] + pad) * (wh[1] + pad)
        return np.sqrt(sz2)

    # size penalty
    target_sz_in_crop = target_sz * scale_x
    s_c = change(sz(delta[2, :], delta[3, :]) / (sz_wh(target_sz_in_crop)))  # scale penalty
    r_c = change((target_sz_in_crop[0] / target_sz_in_crop[1]) / (delta[2, :] / delta[3, :]))  # ratio penalty

    penalty = np.exp(-(r_c * s_c - 1) * p.penalty_k)
    pscore = penalty * score

    # cos window (motion model)
    pscore = pscore * (1 - p.window_influence) + window * p.window_influence
    best_pscore_id = np.argmax(pscore)

    pred_in_crop = delta[:, best_pscore_id] / scale_x
    lr = penalty[best_pscore_id] * score[best_pscore_id] * p.lr  # lr for OTB

    res_x = pred_in_crop[0] + target_pos[0]
    res_y = pred_in_crop[1] + target_pos[1]

    res_w = target_sz[0] * (1 - lr) + pred_in_crop[2] * lr
    res_h = target_sz[1] * (1 - lr) + pred_in_crop[3] * lr

    target_pos = np.array([res_x, res_y])
    target_sz = np.array([res_w, res_h])

    # for Mask Branch
    if mask_enable:
        best_pscore_id_mask = np.unravel_index(best_pscore_id, (5, p.score_size, p.score_size))
        delta_x, delta_y = best_pscore_id_mask[2], best_pscore_id_mask[1]

        if refine_enable:
            mask = net.track_refine((delta_y, delta_x)).to(device).sigmoid().squeeze().view(
                p.out_size, p.out_size).cpu().data.numpy()
        else:
            mask = mask[0, :, delta_y, delta_x].sigmoid(). \
                squeeze().view(p.out_size, p.out_size).cpu().data.numpy()

        def crop_back(image, bbox, out_sz, padding=-1):
            a = (out_sz[0] - 1) / bbox[2]
            b = (out_sz[1] - 1) / bbox[3]
            c = -a * bbox[0]
            d = -b * bbox[1]
            mapping = np.array([[a, 0, c],
                                [0, b, d]]).astype(np.float)
            crop = cv2.warpAffine(image, mapping, (out_sz[0], out_sz[1]),
                                  flags=cv2.INTER_LINEAR,
                                  borderMode=cv2.BORDER_CONSTANT,
                                  borderValue=padding)
            return crop

        s = crop_box[2] / p.instance_size
        sub_box = [crop_box[0] + (delta_x - p.base_size / 2) * p.total_stride * s,
                   crop_box[1] + (delta_y - p.base_size / 2) * p.total_stride * s,
                   s * p.exemplar_size, s * p.exemplar_size]
        s = p.out_size / sub_box[2]
        back_box = [-sub_box[0] * s, -sub_box[1] * s, state['im_w'] * s, state['im_h'] * s]
        mask_in_img = crop_back(mask, back_box, (state['im_w'], state['im_h']))

        target_mask = (mask_in_img > p.seg_thr).astype(np.uint8)
        if cv2.__version__[-5] == '4':
            contours, _ = cv2.findContours(target_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        else:
            _, contours, _ = cv2.findContours(target_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        cnt_area = [cv2.contourArea(cnt) for cnt in contours]
        if len(contours) != 0 and np.max(cnt_area) > 100:
            contour = contours[np.argmax(cnt_area)]  # use max area polygon
            polygon = contour.reshape(-1, 2)
            # pbox = cv2.boundingRect(polygon)  # Min Max Rectangle
            prbox = cv2.boxPoints(cv2.minAreaRect(polygon))  # Rotated Rectangle

            # box_in_img = pbox
            rbox_in_img = prbox
        else:  # empty mask
            location = cxy_wh_2_rect(target_pos, target_sz)
            rbox_in_img = np.array([[location[0], location[1]],
                                    [location[0] + location[2], location[1]],
                                    [location[0] + location[2], location[1] + location[3]],
                                    [location[0], location[1] + location[3]]])

    target_pos[0] = max(0, min(state['im_w'], target_pos[0]))
    target_pos[1] = max(0, min(state['im_h'], target_pos[1]))
    target_sz[0] = max(10, min(state['im_w'], target_sz[0]))
    target_sz[1] = max(10, min(state['im_h'], target_sz[1]))

    state['target_pos'] = target_pos
    state['target_sz'] = target_sz
    state['score'] = score[best_pscore_id]
    state['mask'] = mask_in_img if mask_enable else []
    state['ploygon'] = rbox_in_img if mask_enable else []
    return state


weights_path = "./core/siammask/weights"
resume = os.path.join(weights_path, "SiamMask_DAVIS.pth")
config = os.path.join(weights_path, "config_davis.json")
base_path = "./data/tennis"
camera = True
enable_mask = False

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print(device)
torch.backends.cudnn.benchmark = True
cfg = json.load(open(config))

from core.siammask.models.custom import Custom

siammask = Custom(anchors=cfg['anchors'])
siammask = load_pretrain(siammask, resume)

siammask.eval().to(device)

def url_to_img(url):
  img = Image.open(BytesIO(requests.get(url).content))
  return np.array(img)

# drone = tello.Tello()
# drone.connect()
#
# drone.for_back_velocity = 0
# drone.left_right_velocity = 0
# drone.up_down_velocity = 0
# drone.yaw_velocity = 0
# drone.streamoff()
# drone.streamon()
tracker_output_cmd_vel.data = [0,0,0,0]

# cap = drone.get_frame_read()
# vid = drone.get_video_capture()

image_url = "http://192.168.2.135:8005/html/cam_pic.php?time=1633523000367&pDelay=40000"

frame = url_to_img(image_url)  #from url

frame_width = frame.shape[1]
frame_height = frame.shape[0]

# frame_width = int(vid.get(cv2.CAP_PROP_FRAME_WIDTH))
# frame_height = int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT))
frame_fps = 20 #int(cap.get(5))
# print(frame_fps)

frame_x = int(frame_width / 2)
frame_y = int(frame_height / 2)
frame_area = int(frame_width * frame_height)

deadZone = 70

X_min = int(frame_width / 2) - deadZone
X_max = int(frame_width / 2) + deadZone
Y_min = int(frame_height / 2) - deadZone
Y_max = int(frame_height / 2) + deadZone

trackingOutput = cv2.VideoWriter('Tracking Output data/Siammask_on_Mackrel_feed_'+str(time.localtime().tm_mday)+'-'+str(time.localtime().tm_mon)+'_'+str(time.localtime().tm_hour)+':'+str(time.localtime().tm_min)+':'+str(time.localtime().tm_sec)+'.avi', cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), frame_fps,
                      (frame_width, frame_height))
trainingdatafeed = cv2.VideoWriter('Training data/Drone_feed_'+str(time.localtime().tm_mday)+'-'+str(time.localtime().tm_mon)+'_'+str(time.localtime().tm_hour)+':'+str(time.localtime().tm_min)+':'+str(time.localtime().tm_sec)+'.avi', cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), frame_fps,
                      (frame_width, frame_height))
track = False
key = ' '

pid_x = [0.2, 0, 0.0]
def track_object_x(box_x, pid_x, xPreviousError):
    xCurrentError = box_x - frame_x
    xSpeed = pid_x[0] * xCurrentError + pid_x[2] * (xCurrentError - xPreviousError)
    xSpeed = int(np.clip(xSpeed, -50, 50))

    if box_x != 0:
        # Set Yaw Velocity = xSpeed
        #pass
        tracker_output_cmd_vel.data[3] = -1.0 * xSpeed      #Negated due to axis inversion on mackrel
    else:
        # Set Yaw_velocity = 0
        # Set For_Back_velocity = 0
        # Set Left_Right_Velocity = 0
        # Set Up_Down_Velocity = 0
        xCurrentError = 0
        # tracker_output_cmd_vel.data = [0,0,0,0]
        tracker_output_cmd_vel.data[3] = 0
    # Send Velocity Commands to Drone
    return xCurrentError


pid_y = [1.5, 0, 0.1]
pid_f = [0.5, 0, 0.01]
def track_object_y(box_y, pid_y, yPreviousError, mode):
    yCurrentError = (frame_y - box_y)
    # ySpeed = pid_y[0] * yCurrentError + pid_y[2] * (yCurrentError - yPreviousError)
    # ySpeed = int(np.clip(ySpeed, -300, 300))

    if box_y != 0:
        if mode == 'surveillance':
            ### Move Up/Down to maintain y-error = 0 in the surveillance mode
            ySpeed = pid_y[0] * yCurrentError + pid_y[2] * (yCurrentError - yPreviousError)
            ySpeed = int(np.clip(ySpeed, -300, 300))
            tracker_output_cmd_vel.data[2] = ySpeed
            tracker_output_cmd_vel.data[0] = 0
        if mode == 'follow':
            ### Move Forward/Backward to maintain y-error = 0 in the FOLLOW mode
            ySpeed = pid_f[0] * yCurrentError + pid_f[2] * (yCurrentError - yPreviousError)
            ySpeed = int(np.clip(ySpeed, -300, 300))
            tracker_output_cmd_vel.data[0] = ySpeed
            tracker_output_cmd_vel.data[2] = 0
    else:
        yCurrentError = 0
        tracker_output_cmd_vel.data[2] = 0
        tracker_output_cmd_vel.data[0] = 0
    return yCurrentError


# pid_h = [1.5, 0, 0.1]
# surveillanceHeight = 30
# followHeight = 15
# def set_drone_h(actual_h, pid_h, hPreviousError, mode):
#     if mode == 'disenage':
#         required_h = surveillanceHeight
#     if mode == 'follow':
#         required_h = followHeight
#     hCurrentError = required_h - actual_h
#     hSpeed = pid_h[0] * hCurrentError + pid_h[2] * (hCurrentError - hPreviousError)
#     hSpeed = int(np.clip(hSpeed, -300, 300))
#
#     if actual_h != 0:
#         ###
#         tracker_output_cmd_vel.data[2] = hSpeed
#     else:
#         hCurrentError = 0
#         tracker_output_cmd_vel.data[2] = 0
#     return hCurrentError


#
# pid_area = [0.001, 0, 0.0]
# def track_object_area(box_area, pid_area, areaPreviousError, mode):
#     requiredArea = 0
#     forwardSpeed = 0
#     if mode == 'surveillance':
#         requiredArea = box_area
#     if mode == 'follow':
#         requiredArea = 0.3 * frame_area
#     if mode == 'attack':
#         requiredArea = 2 * frame_area
#
#
#     areaCurrentError = requiredArea - box_area
#
#     if areaCurrentError > 0:
#         forwardSpeed = pid_area[0] * areaCurrentError + pid_area[2] * abs(areaCurrentError - areaPreviousError)
#     if areaCurrentError < 0:
#         forwardSpeed = pid_area[0] * areaCurrentError - pid_area[2] * abs(areaCurrentError - areaPreviousError)
#
#     forwardSpeed = int(np.clip(forwardSpeed, -450, 450))
#
#     if box_area != 0:
#         # Set FOr_Back_Velocity = forwardSpeed
#         #pass
#         tracker_output_cmd_vel.data[0] = forwardSpeed
#         # tracker_output_cmd_vel.data[2] = -1 * forwardSpeed
#     else:
#         # Set Yaw_velocity = 0
#         # Set For_Back_velocity = 0
#         # Set Left_Right_Velocity = 0
#         # Set Up_Down_Velocity = 0
#         areaCurrentError = 0
#         tracker_output_cmd_vel.data[0] = 0
#         # tracker_output_cmd_vel.data = [0, 0, 0, 0]
#     # Send Velocity Commands to Drone
#
#     print(box_area, requiredArea, areaCurrentError, forwardSpeed)
#     return areaCurrentError

# normal_forward_velocity = 40
# terminal_forward_velocity = 100
# up_down_velocity = 50
#
#
# def move_drone(frame, box_x, box_y, box_area, mode):
#     cx = box_x
#     cy = box_y
#     currentArea = box_area
#     if mode == 'surveillance':
#         requiredArea = currentArea
#     if mode == 'attack':
#         velocity = terminal_forward_velocity
#         requiredArea = frame_area
#     if mode == 'follow':
#         velocity = normal_forward_velocity
#         requiredArea = int(0.05 * frame_area)
#         # 0.5 for person
#         # 0.01 for bottle, cell phone
#     if cx < X_min:
#         s_1 = 'Rotate Left'
#     if cx > X_max:
#         s_1 = 'Rotate Right'
#     if X_min <= cx <= X_max:
#         # Set Yaw_velocity = 0
#         s_1 = ''
#     if cy < Y_min:
#         s_2 = 'Move Up'
#     if cy > Y_max:
#         s_2 = 'Move Down'
#     if Y_min <= cy <= Y_max:
#         # Set Up_Down_Velocity = 0
#         s_2 = ''
#     if currentArea < requiredArea:
#         # Set For_Back_velocity = velocity
#         s_3 = 'Move Forward'
#         tracker_output_cmd_vel.data[0] = velocity
#     if currentArea >= requiredArea:
#         # Set For_Back_velocity = 0
#         tracker_output_cmd_vel.data[0] = 0
#         s_3 = ''
#
#     # Send Velocity Commands to Drone
#
#     # cv2.putText(frame, s_1 + ' , ' + s_2 + ' , ' + s_3 + ' , ' + mode, (10, 130), 0, 0.75, (255, 255, 255), 2)
#     # cv2.putText(frame, str(box_area) + ' , ' + str(requiredArea) + ' , ' + str(requiredArea - box_area), (10, 180), 0, 0.75, (255, 255, 255), 2)


xPreviousError = 0
yPreviousError = 0
areaPreviousError = 0
hPreviousError = 0

width_padding = int(0.03 * frame_width)
height_padding = int(0.03 * frame_height)
box_x_list = []
box_y_list = []

requiredArea = 0
mode_list = ['surveillance']
# flight_mode = [' ']
rospy.init_node('siam_mask', anonymous=True)
pub = rospy.Publisher('traget_tracker_to_mavros', Float64MultiArray, queue_size=100)
status_message = ''
info_message = ''

rate = rospy.Rate(20)

while key != ord("q"):
    frame = url_to_img(image_url)  # from url

    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

    start_time = time.time()
    # ret, frame = cap.read()
    # frame = cap.frame
    # frame = frame[:,0:frame.shape[1]//2,:]

    trainingdatafeed.write(frame)

    if key == ord("r"):
        cv2.namedWindow("SiamMask", cv2.WND_PROP_FULLSCREEN)
        # cv2.setMouseCallback('SiamMask', click_event)
        # Select ROI
        x, y, w, h = cv2.selectROI('SiamMask', frame, False, False)
        target_pos = np.array([x + w / 2, y + h / 2])
        target_sz = np.array([w, h])
        state = siamese_init(frame, target_pos, target_sz, siammask, cfg['hp'], device)  # init tracker
        track = True
    if key == ord("c"):
        track = False
        tracker_output_cmd_vel.data = [0, 0, 0, 0]
    if key == ord("t"):
        # send the command to tak off
        pass
    if key == ord("l"):
        # send the command to Land
        tracker_output_cmd_vel.data = [0, 0, 0, 0]
        pass

    if track:
        # if key == ord("a"):
        #     mode_list.append('attack')
        if key == ord("f"):
            mode_list.append('follow')
        if key == ord("s"):
            mode_list.append('surveillance')

        cv2.line(frame, (X_min, Y_min), (X_min, Y_max), (255, 255, 0), 1)
        cv2.line(frame, (X_max, Y_min), (X_max, Y_max), (255, 255, 0), 1)
        cv2.line(frame, (X_min, Y_min), (X_max, Y_min), (255, 255, 0), 1)
        cv2.line(frame, (X_min, Y_max), (X_max, Y_max), (255, 255, 0), 1)

        if enable_mask:
            state = siamese_track(state, frame, True, True, device, False)  # track
            location = state['ploygon'].flatten()
            mask = state['mask'] > state['p'].seg_thr
            frame[:, :, 2] = (mask > 0) * 255 + (mask == 0) * frame[:, :, 2]
            cv2.polylines(frame, [np.int0(location).reshape((-1, 1, 2))], True, (0, 255, 0), 3)
            bbox_list = list(np.int0(location).reshape((-1, 1, 2)))

            b_x = []
            b_y = []
            for coord in bbox_list:
                b_x.append(coord[0][0])
                b_y.append(coord[0][1])
            box_x = int(sum(b_x)/4)
            box_y = int(sum(b_y)/4)
            box_area = abs(int(abs(b_x[0] * b_y[1] - b_x[1] * b_y[0] + b_x[1] * b_y[2] - b_x[2] * b_y[1] + b_x[2] * b_y[3] - b_x[3] * b_y[2] + b_x[3] * b_y[0] - b_x[0] * b_y[3]) / 4))
            box_width = int(max(b_x) - min(b_x))
            box_height = int(max(b_y) - min(b_y))

        else:
            state = siamese_track(state, frame, mask_enable=False, refine_enable=True, device=device,
                                  debug=False)  # track
            cv2.rectangle(frame, tuple((state['target_pos'] - state['target_sz'] // 2).astype(np.int)),
                          tuple((state['target_pos'] + state['target_sz'] // 2).astype(np.int)), (255, 255, 0), 3)
            t_l_x = tuple((state['target_pos'] - state['target_sz'] // 2).astype(np.int))[0]
            t_l_y = tuple((state['target_pos'] - state['target_sz'] // 2).astype(np.int))[1]
            b_r_x = tuple((state['target_pos'] + state['target_sz'] // 2).astype(np.int))[0]
            b_r_y = tuple((state['target_pos'] + state['target_sz'] // 2).astype(np.int))[1]
            box_x = int((t_l_x + b_r_x) / 2)
            box_y = int((t_l_y + b_r_y) / 2)
            box_area = abs(int((b_r_x - t_l_x) * (b_r_y - t_l_y)))
            box_width = int(b_r_x - t_l_x)
            box_height = int(b_r_y - t_l_y)

        box_x_list.append(box_x)
        box_y_list.append(box_y)
        # print('box_x : ', box_x, '  box_y : ', box_y, '  box_area : ', box_area)

        ####### get the actual height from MavRos as feedback
        # actual_h = 0 ########################################
        #####################################################

        xPreviousError = track_object_x(box_x, pid_x, xPreviousError)
        yPreviousError = track_object_y(box_y, pid_y, yPreviousError, mode_list[-1])
        # hPreviousError = set_drone_h(actual_h, pid_h, hPreviousError, mode_list[-1])
        # areaPreviousError = track_object_area(box_area, pid_area, areaPreviousError, mode_list[-1])

        status_message = 'Tracking'
        if box_x < width_padding or box_x > frame_width - width_padding or box_y < height_padding or box_y > frame_height - height_padding:
            print("Tracking lost")
            print("reason : Out of the frame")
            status_message = 'Tracking Lost'
            info_message = 'Press "R" to select an object'
            track = False
            tracker_output_cmd_vel.data = [0, 0, 0, 0]

        if len(box_x_list) > 2:
            box_x_error = box_x_list[-1] - box_x_list[-2]
            box_y_error = box_y_list[-1] - box_y_list[-2]
            if box_x_error > 1.5 * box_width or box_y_error > 1.5 * box_height:
                print("Tracking lost")
                print("reason : Object switched")
                status_message = 'Tracking Lost'
                info_message = 'Press "R" to select an object'
                track = False
                tracker_output_cmd_vel.data = [0, 0, 0, 0]
                # pub.publish(tracker_output_cmd_vel)

        pub.publish(tracker_output_cmd_vel)
        print(tracker_output_cmd_vel.data)
        rate.sleep()

    # cv2.putText(frame, "Drone Battery: {}".format(str(drone.get_battery())), (10, 30), 0, 0.75, (255, 255, 255), 2)

    fps = round((1.0 / (time.time() - start_time)), 2)
    frame_fps = fps
    # cv2.putText(frame, "FPS: {}".format(str(fps)), (10, 80), 0, 0.75, (255, 255, 255), 2)
    # cv2.putText(frame, "Status: {}".format(status_message), (10, 80), 0, 0.75, (0, 0, 0), 2)
    # cv2.putText(frame, info_message, (10, 130), 0, 0.75, (0, 0, 0), 2)
    cv2.imshow('SiamMask', frame)
    trackingOutput.write(frame)
    key = cv2.waitKey(1)

if key == 'q':
    tracker_output_cmd_vel.data = [0,0,0,0]
    pub.publish(tracker_output_cmd_vel)
    time.sleep(3)

# drone.land()
# cap.release()
trainingdatafeed.release()
trackingOutput.release()
cv2.destroyAllWindows()
