import cv2
import time
cap = cv2.VideoCapture(0)

ret, frame = cap.read()
frame_width = int(cap.get(3))
frame_height = int(cap.get(4))
frame_fps = int(cap.get(5))

save_time = time.time()
out = cv2.VideoWriter('Mackrel feed_' + str(time.time()) +'.avi', cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), frame_fps,
                      (frame_width, frame_height))

key = ' '
while key != ord("q"):
    start_time = time.time()
    ret, frame = cap.read()
    if ret == False:
        print("No camera attached")
        break
    fps = round(1.0 / (time.time() - start_time), 2)
    cv2.putText(frame, "FPS: {}".format(str(fps)), (10, 30), 0, 0.75, (255, 255, 255), 2)
    cv2.imshow('Mackrel feed', frame)
    out.write(frame)
    key = cv2.waitKey(1)

cap.release()
out.release()
cv2.destroyAllWindows()