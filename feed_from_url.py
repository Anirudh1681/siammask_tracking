
import requests
import numpy as np
from io import BytesIO
from PIL import Image
import cv2

def url_to_img(url):
  img = Image.open(BytesIO(requests.get(url).content))
  return np.array(img)

while True:
    img = url_to_img('http://192.168.2.135:8005/html/cam_pic.php?time=1633341820749&pDelay=40000')

    cv2.imshow("Test",img)
    cv2.waitKey(1)
